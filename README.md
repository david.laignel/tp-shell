# Les clés sont sur le bureau
--------
## 1. Principes
Le Tp est un 'escape room' qui peut se joueur en équipe de 3 ou en individuel.
### A. Le matériel
- Un ordinateur  
  - avec un système d'exploitation libre (debian sous virtual box pour le test sur mon ordinateur personnel, clé bootable ...),
  - sans interface graphique ou sans souris pour rendre obligatoire l'usage des commandes,
  - un répertoire "cles" contenu dans le répertoire "bureau",
  - le mode console ouvert et l'utilisateur défini avec les droits nécéessaires( propriétaire du dossier clés par exemple) pour pouvoir effectuer le tp.

- Le répertoire "cles" contient :  
  - un répertoire "cle1" contenant un fichier .cle1 ayant pour contenu 01011010,
  - un répertoire "cle2" contenant un fichier cle2.txt dont les droits sont fixés à 0. Le fichier cle2.txt a pour contenu 00101110
  - un répertoire "cle3" contenant un fichier cle3.txt vide mais dont les droits sont fixés à 751

- des indices ( post-it , photos, papiers qui traînent ....)  
  
  - Indice 1 : "Je suis cachée.",
  - Indice 2 : "Rétablissez mes droits.",
  - Indice 3 : "Mes droits sont la clé.",
  - Indice 4 : " 15<sup>7</sup> combinaisons possibles. ",
  - Indice 5 : "Les clés sont sur le bureau."
  - Indice supplémentaire éventuel : "cle1 =11100010 , cle2 = 00111100 , cle3 = 574 alors cle = E23C23E"
  - éventuellement des petites fiches rappellant les principales commandes nécessaires.

- un système de validation de la clé ( le prof, une vraie serrure , un outil de validation sur le net ( page html avec un formulaire et un script php de validation ?).

Les messages des indices peuvent être éventuellement codés si l'on veut revenir sur certains types d'encodage.  
### B. Le scénario.

La porte de la salle d'informatique vient d'être dotée d'une nouvelle serrure.  
![clavier à 15 touches](images/serrure.jpg).  
"jour299" by Guillaume Brialon is licensed under CC BY-NC-SA 2.0  
Malheureusement la porte vient de se claquer et vous ne connaissez pas le code nécessaire pour sortir.    
Heureusement pour vous, le prof d'informatique qui n'a pas beaucoup de mémoire et oublit souvent le code a laissé différents indices pour retrouver la clé pour sortir.

### C. Déroulé et correction.

#### cle 1
![copie-écran-commandes](/images/cle1-A.png)
![copie-écran-commandes](/images/cle1-B.png)
On recupère la clé : 01011010 soit 5A

#### cle 2
![copie-écran-commandes](/images/cle2-A.png)
![copie-écran-commandes](/images/cle2-B.png)
![copie-écran-commandes](/images/cle2-C.png)
![copie-écran-commandes](/images/cle2-D.png)
On recupère la clé : 00101110 soit 2E

#### cle 3
![copie-écran-commandes](/images/cle3.png)
Le CHMOD est à 700 soit 2BC

La clé est donc 5A2E2BC





